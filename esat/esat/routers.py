from rest_framework import routers
from telemetry.viewsets import SateliteViewSet, SensorViewSet, VariableViewSet, MeasureViewSet

router = routers.DefaultRouter()

router.register(r'satellites', SateliteViewSet)
router.register(r'sensors', SensorViewSet)
router.register(r'variables', VariableViewSet)
router.register(r'measurements', MeasureViewSet)
