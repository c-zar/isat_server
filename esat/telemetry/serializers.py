from .models import Satelite, Sensor, Variable, Measure
from rest_framework import serializers
from django.conf import settings
from dry_rest_permissions.generics import DRYPermissionsField

class MeasureSerializer(serializers.ModelSerializer):
    #permissions = DRYPermissionsField(global_only=True)
    class Meta:
        model = Measure
        fields = ['value','date']
    
    def get_permissions_map(self, created):
        current_user = self.context['request'].user
        readers = Group.objects.get(name='readers')
        supervisors = Group.objects.get(name='supervisors')
        return {
            'view_post': [current_user, readers],
            'change_post': [current_user],
            'delete_post': [current_user, supervisors]
        }        



class VariableSerializer(serializers.ModelSerializer):
    #permissions = DRYPermissionsField(global_only=True)
    measurements = MeasureSerializer(many=True)
    class Meta:
        model = Variable
        fields = ['name','unity','date','measurements']
    
    def get_permissions_map(self, created):
        current_user = self.context['request'].user
        readers = Group.objects.get(name='readers')
        supervisors = Group.objects.get(name='supervisors')
        return {
            'view_post': [current_user, readers],
            'change_post': [current_user],
            'delete_post': [current_user, supervisors]
        }     
    def create(self, validated_data):
        measure_validated_data = validated_data.pop('measurements')
        variable = Variable.objects.create(**validated_data)
        measurements_serializer = self.fields['measurements']
        for each in measure_validated_data:
            each['variable'] = variable
        measurements = variables_serializer.create(measure_validated_data)
        return variable
        
class SensorSerializer(serializers.ModelSerializer):
    #permissions = DRYPermissionsField(global_only=True)
    variables = VariableSerializer(many=True)
    class Meta:
        model = Sensor
        fields = ['name','variables']
    
    def get_permissions_map(self, created):
        current_user = self.context['request'].user
        readers = Group.objects.get(name='readers')
        supervisors = Group.objects.get(name='supervisors')
        return {
            'view_post': [current_user, readers],
            'change_post': [current_user],
            'delete_post': [current_user, supervisors]
        }

    def create(self, validated_data):
        variable_validated_data = validated_data.pop('variables')
        sensor = Sensor.objects.create(**validated_data)
        variables_serializer = self.fields['variables']
        for each in variable_validated_data:
            each['sensor'] = sensor
        variables = sensores_serializer.create(variable_validated_data)
        return sensor
    
class SatelliteSerializer(serializers.ModelSerializer):
    #permissions = DRYPermissionsField(global_only=True)
    sensors = SensorSerializer(many=True)
    class Meta:
        model = Variable
        fields = ['name','sensors']
    
    def get_permissions_map(self, created):
        current_user = self.context['request'].user
        readers = Group.objects.get(name='readers')
        supervisors = Group.objects.get(name='supervisors')
        return {
            'view_post': [current_user, readers],
            'change_post': [current_user],
            'delete_post': [current_user, supervisors]
        }        