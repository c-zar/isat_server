from django.db import models
from django.utils.translation import gettext as _
from django.conf import settings

class Satelite(models.Model):
    name = models.CharField(default='',max_length=50)
    propietary = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    class Meta:
        verbose_name = _("satelite")
        verbose_name_plural = _("satellites")

    def __str__(self):
        return self.name 
    def get_permissions_map(self, created):
        current_user = self.context['request'].user
        readers = Group.objects.get(name='readers')
        supervisors = Group.objects.get(name='supervisors')

        return {
            'view_post': [current_user, readers],
            'change_post': [current_user],
            'delete_post': [current_user, supervisors]
        }

class Sensor(models.Model):
    name = models.CharField(default='', max_length=50)
    satellites = models.ForeignKey(Satelite, related_name='sensors', on_delete=models.CASCADE)
    propietary = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    class Meta:
        verbose_name = _("sensor")
        verbose_name_plural = _("sensors")
        
    def __str__(self):
        return self.name

    def get_permissions_map(self, created):
        current_user = self.context['request'].user
        readers = Group.objects.get(name='readers')
        supervisors = Group.objects.get(name='supervisors')

        return {
            'view_post': [current_user, readers],
            'change_post': [current_user],
            'delete_post': [current_user, supervisors]
        }

class Variable(models.Model):
    name = models.CharField(default='',max_length=50)
    unity = models.CharField(default='',max_length=10)
    date = models.DateTimeField(auto_now=True)
    sensors = models.ForeignKey(Sensor, related_name='variables', on_delete=models.CASCADE)
    propietary = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = _("Variable")
        verbose_name_plural = _("Variables")

    def __str__(self):
        return self.name

    def get_permissions_map(self, created):
        current_user = self.context['request'].user
        readers = Group.objects.get(name='readers')
        supervisors = Group.objects.get(name='supervisors')

        return {
            'view_post': [current_user, readers],
            'change_post': [current_user],
            'delete_post': [current_user, supervisors]
        }        
        
class Measure(models.Model):
    value = models.FloatField(default=0)
    date = models.DateTimeField(auto_now=True)
    variables = models.ForeignKey(Variable, related_name='measurements', on_delete=models.CASCADE)
    propietary = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    
    class Meta:
        verbose_name = _("Measure")
        verbose_name_plural = _("Measuremets")

    def __str__(self):
        return str(self.date) +' ---> '+ str(self.value)

    def get_permissions_map(self, created):
        current_user = self.context['request'].user
        readers = Group.objects.get(name='readers')
        supervisors = Group.objects.get(name='supervisors')

        return {
            'view_post': [current_user, readers],
            'change_post': [current_user],
            'delete_post': [current_user, supervisors]
        }        
        