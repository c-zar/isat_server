from django.contrib import admin

from .models import Satelite, Sensor, Variable, Measure

admin.site.register(Satelite)
admin.site.register(Sensor)
admin.site.register(Variable)
admin.site.register(Measure)
