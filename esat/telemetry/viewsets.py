from rest_framework import viewsets, generics, filters
from .models import Satelite, Sensor, Variable, Measure
from .serializers import SatelliteSerializer, SensorSerializer, VariableSerializer, MeasureSerializer
from dry_rest_permissions.generics import DRYPermissions
from django.db.models import Q
from dry_rest_permissions.generics import DRYPermissionFiltersBase
from django_filters.rest_framework import DjangoFilterBackend

class TelemetryFilterBackend(DRYPermissionFiltersBase):
    
    def filter_list_queryset(self, request, queryset, view):
        """
        Limits all list requests to only be seen by the owners or creators.
        """
        return queryset.filter(Q(propietary=request.user) | Q(propietary=request.user))

class MeasureViewSet(viewsets.ModelViewSet):
    queryset = Measure.objects.all()
    serializer_class = MeasureSerializer
    #permission_classes = (DRYPermissions,)
    
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, 
                       DjangoFilterBackend, TelemetryFilterBackend)
    ordering_fields = ('id','date')
    ordering = ('date',)
    filter_fields = ('id', 'date')
    search_fields = ('date',)
    #pagination_class = MiniResultsSetPagination

 
class VariableViewSet(viewsets.ModelViewSet):
    queryset = Variable.objects.all()
    serializer_class = VariableSerializer
    #permission_classes = (DRYPermissions,)
    
    #filter_backends = (filters.SearchFilter, filters.OrderingFilter, 
    #                   DjangoFilterBackend, TelemetryFilterBackend)
    ordering_fields = ('id','name','date')
    ordering = ('date',)
    filter_fields = ('id', 'name', 'date')
    search_fields = ('name','date',)
    #pagination_class = MiniResultsSetPagination
    
    def post(self, request, *args, **kwargs):
        serializer = VariableSerializer(data=request.data)
        if serializer.is_valid():
            variable = serializer.save()
            serializer = VariableSerializer(variable)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class SensorViewSet(viewsets.ModelViewSet):
    queryset = Sensor.objects.all()
    serializer_class = SensorSerializer
    #permission_classes = (DRYPermissions,)
    
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, 
                       DjangoFilterBackend, TelemetryFilterBackend)
    ordering_fields = ('id','name','variables')
    ordering = ('name',)
    filter_fields = ('id', 'name', 'variables')
    search_fields = ('name',)
    #pagination_class = MiniResultsSetPagination
    
    def post(self, request, *args, **kwargs):
        serializer = SensorSerializer(data=request.data)
        if serializer.is_valid():
            sensor = serializer.save()
            serializer = SensorSerializer(sensor)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
class SateliteViewSet(viewsets.ModelViewSet):
    queryset = Satelite.objects.all()
    serializer_class = SatelliteSerializer
    #permission_classes = (DRYPermissions,)
    
    filter_backends = (filters.SearchFilter, filters.OrderingFilter, 
                       DjangoFilterBackend, TelemetryFilterBackend)
    ordering_fields = ('id','name','sensors')
    ordering = ('name',)
    filter_fields = ('id', 'name',)
    search_fields = ('name',)
    #pagination_class = MiniResultsSetPagination